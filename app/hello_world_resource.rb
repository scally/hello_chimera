class HelloWorldResource
  extend Resource

  path 'hello'
  respond_with :text

  action :say_hello do
    get
    returns String
  end
  def say_hello
    'hello world'.to_java
  end
end
