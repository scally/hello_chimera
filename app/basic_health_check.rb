java_import 'com.codahale.metrics.health.HealthCheck'

class BasicHealthCheck < HealthCheck
  def check
    Result.healthy
  end
end
