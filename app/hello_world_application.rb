require 'app/hello_world_resource'
require 'app/sayings_resource'
require 'app/basic_health_check'

class HelloWorldApplication < ChimeraApplication
  display_name 'HelloWorldApp'
  resources HelloWorldResource, SayingsResource
  health_checks BasicHealthCheck
end
